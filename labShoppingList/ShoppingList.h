//---------------------------------------------------------------------------

#ifndef ShoppingListH
#define ShoppingListH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.MultiView.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiList;
	TTabItem *tiItem;
	TToolBar *tbList;
	TSpeedButton *sbCheck;
	TSpeedButton *sbEdit;
	TLabel *laCaption;
	TButton *buAdd;
	TButton *buPopuv;
	TListView *lv;
	TMultiView *mvPopuv;
	TListBox *lbPopuv;
	TListBoxItem *lbiDeleteAll;
	TListBoxItem *lbiHelp;
	TListBoxItem *lbiAbout;
	TToolBar *ToolBar1;
	TLabel *laCaption2;
	TButton *buDel;
	TButton *buBack;
	TGridPanelLayout *GridPanelLayout1;
	TScrollBox *ScrollBox1;
	TLayout *Layout1;
	TLabel *Label1;
	TButton *buImagePrev;
	TButton *buImageNext;
	TButton *buImagesClear;
	TGlyph *Glyph;
	TLabel *Label2;
	TEdit *edText;
	TLabel *Label3;
	TComboBox *cbHeaderText;
	TLabel *Label4;
	TMemo *meDetail;
	TSwitch *swCheckmark;
	TLabel *Label5;
	TGridPanelLayout *GridPanelLayout2;
	TButton *buSave;
	TButton *buCancel;
	TBindingsList *BindingsList1;
	TBindSourceDB *BindSourceDB1;
	TLinkListControlToField *LinkListControlToField1;
	TLinkControlToField *LinkControlToField1;
	TLinkControlToField *LinkControlToField2;
	TLinkControlToField *LinkControlToField3;
	TLinkFillControlToField *LinkFillControlToField1;
	TLinkPropertyToField *LinkPropertyToFieldImageIndex;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall lvItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall lvUpdateObject(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buAddClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall buDelClick(TObject *Sender);
	void __fastcall buSaveClick(TObject *Sender);
	void __fastcall buCancelClick(TObject *Sender);
	void __fastcall buImagePrevClick(TObject *Sender);
	void __fastcall buImageNextClick(TObject *Sender);
	void __fastcall buImagesClearClick(TObject *Sender);
	void __fastcall GlyphChanged(TObject *Sender);


private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
