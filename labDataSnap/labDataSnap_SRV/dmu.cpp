//----------------------------------------------------------------------------
#pragma hdrstop
#include <tchar.h>
#include <stdio.h>
#include <memory>
#include <string>
#include "dmuAAA.h"

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

//---------------------------------------------------------------------------
char const *sPortInUse = "- Error: Port already in use\n";
char const *sPortSet = "- Port set to %d\n";
char const *sPortNotSet = "- Port could not be set\n";
char const *sServerRunning = "- The Server is already running\n";
char const *sStartingServer = "- Starting Server\n";
char const *sServerIsRunning = "- Server Running\n";
char const *sStoppingServer = "- Stopping Server\n";
char const *sServerStopped = "- Server Stopped\n";
char const *sServerNotRunning = "- The Server is not running\n";
char const *sInvalidCommand = "- Error: Invalid Command\n";
char const *sIndyVersion = "- Indy Version: ";
char const *sActive = "- Active: ";
char const *sTCPIPPort = "- TCP/IP Port: ";
char const *sHTTPPort = "- HTTP Port: ";
char const *sHTTPSPort = "- HTTPS Port: ";
char const *sSessionID = "- Session ID CookieName: ";
char const *sCommands = "Enter a Command: \n"
	"   - \"start\" to start the server\n"
	"   - \"stop\" to stop the server\n"
	"   - \"set port -t\" to change the TCP/IP default port\n"
	"   - \"set port -h\" to change the HTTP default port\n"
	"   - \"set port -s\" to change the HTTPS default port\n"
	"   - \"status\" for Server status\n"
	"   - \"help\" to show commands\n"
	"   - \"exit\" to close the application\n";
char const *sArrow = "->";
char const *sCommandStart = "start";
char const *sCommandStop = "stop";
char const *sCommandStatus = "status";
char const *sCommandHelp = "help";
char const *sCommandSetTCPIPPort = "set port -t";
char const *sCommandSetHTTPPort = "set port -h";
char const *sCommandSetHTTPSPort = "set port -s";
char const *sCommandExit = "exit";

typedef enum {TCPIP, HTTP, HTTPS} TDSProtocol;
//---------------------------------------------------------------------------

Tdm *dm;

//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//----------------------------------------------------------------------------
void __fastcall Tdm::DSServerClass1GetClass(TDSServerClass *DSServerClass,
          TPersistentClass &PersistentClass)
{
	PersistentClass =  __classid(TdmAAA);
}
//----------------------------------------------------------------------------

void writeCommands()
{
	printf(sCommands);
	printf(sArrow);
}

void startServer(std::unique_ptr<Tdm>const& module)
{
	if (!module->DSServer1->Started) {
		try {
			module->DSServer1->Start();
		} catch (Exception &exception) {		
			printf(sPortInUse);		
		}
		
		if (module->DSServer1->Started) printf(sStartingServer);
	}
	else {
		printf(sServerRunning);
	}

	printf(sArrow);
}

void stopServer(std::unique_ptr<Tdm>const& module)
{
	if (module->DSServer1->Started) {
		printf(sStoppingServer);
		module->DSServer1->Stop();
		printf(sServerStopped);
	}
	else {
		printf(sServerNotRunning);
	}

	printf(sArrow);
}

void setPort(std::unique_ptr<Tdm>const& module, int port, TDSProtocol protocol)
{
	if (!module->DSServer1->Started) {	
		switch(protocol) {	
		case TCPIP:
			module->DSTCPServerTransport1->Port = port;
			break;
		
		
					
		}	
		printf(sPortSet, port);
	}
	else {
		printf(sServerRunning);
	}
	printf(sArrow);
}

void writeStatus(std::unique_ptr<Tdm>const& module)
{
	printf("%s %s\n", sActive, (module->DSServer1->Started) ? "true" : "false");
	printf("%s %d\n", sTCPIPPort, module->DSTCPServerTransport1->Port);
    
	printf(sArrow);
}

void runDSServer()
{
	std::wstring wsResponse;
	String sResponse;
	int iPort = 0;

	std::unique_ptr<Tdm> module(new Tdm(NULL));
	
	module->DSServer1->Start();	
	
	if (module->DSServer1->Started) printf(sServerIsRunning);

	writeCommands();
	while (true)
	{
		std::getline(std::wcin, wsResponse);

		sResponse = wsResponse.c_str();
		sResponse = sResponse.LowerCase();

		if (SameText(sResponse, sCommandStart))
			startServer(module);
		else if (SameText(sResponse.SubString(1, strlen(sCommandSetTCPIPPort)), sCommandSetTCPIPPort)) {
			iPort = sResponse.SubString(strlen(sCommandSetTCPIPPort) + 1,
					sResponse.Length() - strlen(sCommandSetTCPIPPort)).Trim().ToInt();

			if (iPort > 0)
				setPort(module, iPort, TCPIP);
			else {
				printf(sPortNotSet);
				printf(sArrow);
			}		
		}
						
						
						
		else if (SameText(sResponse, sCommandStart))
			startServer(module);
		else if (SameText(sResponse, sCommandStop))
			stopServer(module);
		else if (SameText(sResponse, sCommandStatus))
			writeStatus(module);
		else if (SameText(sResponse, sCommandHelp))
			writeCommands();
		else if (SameText(sResponse, sCommandExit)) {
			stopServer(module);
			break;
		}
		else {
			printf(sInvalidCommand);
			printf(sArrow);
		}
	}
}
//----------------------------------------------------------------------------
//---------------------------------------------------------------------------

