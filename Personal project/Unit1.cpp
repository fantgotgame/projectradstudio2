//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buStartClick(TObject *Sender)
{
    tc->ActiveTab = tiPlay1;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::buNextClick(TObject *Sender)
{
	tc->ActiveTab = tiPlay2;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::buGoClick(TObject *Sender)
{
	tc->ActiveTab = tiPlay3;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::buNext2Click(TObject *Sender)
{
	tc->ActiveTab = tiPlay4;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAskClick(TObject *Sender)
{
    tc->ActiveTab = tiPlay5;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNext3Click(TObject *Sender)
{
    tc->ActiveTab = tiEnd1;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buRepeatClick(TObject *Sender)
{
    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buWrittClick(TObject *Sender)
{
    tc->ActiveTab = tiPlay6;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buSixClick(TObject *Sender)
{
    tc->ActiveTab = tiPlay7;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNineClick(TObject *Sender)
{
    tc->ActiveTab = tiPlay8;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buStopClick(TObject *Sender)
{
    tc->ActiveTab = tiPlay9;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buProgClick(TObject *Sender)
{
    tc->ActiveTab = tiPlay10;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buFootballClick(TObject *Sender)
{
    tc->ActiveTab = tiPlay11;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buInviteClick(TObject *Sender)
{
    tc->ActiveTab = tiPlay12;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buLoseClick(TObject *Sender)
{
    tc->ActiveTab = tiPlay13;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAboutClick(TObject *Sender)
{
    tc->ActiveTab = tiAbout;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackToMenuClick(TObject *Sender)
{
    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buRulesClick(TObject *Sender)
{
    tc->ActiveTab = tiRules;
}
//---------------------------------------------------------------------------

