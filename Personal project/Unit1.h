//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TGridPanelLayout *gpl;
	TButton *buStart;
	TButton *buRules;
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiPlay1;
	TTabItem *tiPlay3;
	TButton *buAbout;
	TLayout *lyWelcome;
	TLabel *laWelcome;
	TImage *imEvent;
	TButton *buNext;
	TMemo *meVlog;
	TImage *imSchool;
	TMemo *meSchool;
	TButton *buNext2;
	TTabItem *tiPlay2;
	TImage *imPers;
	TMemo *meHome;
	TButton *buGo;
	TButton *buStop;
	TTabItem *tiPlay4;
	TImage *imGirl;
	TMemo *meGirl;
	TButton *buAsk;
	TButton *buWritt;
	TTabItem *tiPlay5;
	TImage *imFriend;
	TMemo *meFriend;
	TButton *buNext3;
	TTabItem *tiEnd1;
	TImage *imNotFriend;
	TMemo *meNotFriend;
	TButton *buRepeat;
	TTabItem *tiPlay6;
	TImage *imLetter;
	TMemo *meLetter;
	TButton *buSix;
	TButton *buNine;
	TTabItem *tiPlay7;
	TImage *imLunch;
	TMemo *meLunch;
	TButton *buRepeat2;
	TTabItem *tiPlay8;
	TImage *imDinner;
	TMemo *meDinner;
	TButton *buRepeat3;
	TTabItem *tiPlay9;
	TImage *imRoom;
	TMemo *meRoom;
	TButton *buProg;
	TButton *buFootball;
	TTabItem *tiPlay10;
	TImage *imProg;
	TMemo *meProg;
	TButton *buRepeat4;
	TTabItem *tiPlay11;
	TImage *imFootball;
	TMemo *meFootball;
	TButton *buInvite;
	TButton *buLose;
	TTabItem *tiPlay12;
	TImage *Image1;
	TMemo *Memo1;
	TButton *buFinish;
	TTabItem *tiPlay13;
	TImage *Image2;
	TMemo *Memo2;
	TButton *buRepeat5;
	TStyleBook *StyleBook1;
	TTabItem *tiAbout;
	TImage *Image3;
	TLabel *Label1;
	TLabel *Label2;
	TToolBar *ToolBar1;
	TLabel *Label3;
	TTabItem *tiRules;
	TToolBar *ToolBar2;
	TMemo *Memo3;
	TLabel *Label4;
	TButton *buBackToMenu;
	TButton *Button1;
	TImage *Image4;
	void __fastcall buStartClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buNextClick(TObject *Sender);
	void __fastcall buGoClick(TObject *Sender);
	void __fastcall buNext2Click(TObject *Sender);
	void __fastcall buAskClick(TObject *Sender);
	void __fastcall buNext3Click(TObject *Sender);
	void __fastcall buRepeatClick(TObject *Sender);
	void __fastcall buWrittClick(TObject *Sender);
	void __fastcall buSixClick(TObject *Sender);
	void __fastcall buNineClick(TObject *Sender);
	void __fastcall buStopClick(TObject *Sender);
	void __fastcall buProgClick(TObject *Sender);
	void __fastcall buFootballClick(TObject *Sender);
	void __fastcall buInviteClick(TObject *Sender);
	void __fastcall buLoseClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buBackToMenuClick(TObject *Sender);
	void __fastcall buRulesClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
