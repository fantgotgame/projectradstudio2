//---------------------------------------------------------------------------

#ifndef frameEndH
#define frameEndH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TfruEnd : public TFrame
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TButton *buMore;
	TButton *buExit;
private:	// User declarations
public:		// User declarations
	__fastcall TfruEnd(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfruEnd *fruEnd;
//---------------------------------------------------------------------------
#endif
