//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "labEmployeeForm.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "frameContact"
#pragma link "frameEnd"
#pragma link "framePerson"
#pragma link "frameStart"
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::fruEnd1buMoreClick(TObject *Sender)
{
	fm->tc->TabIndex = 1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::fruStart1buYesClick(TObject *Sender)
{
   fm->tc->TabIndex = 1;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::fruStart1buNoClick(TObject *Sender)
{
	fm->Close();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::fruEnd1buExitClick(TObject *Sender)
{
	fm->Close();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buNext1Click(TObject *Sender)
{
    tc->ActiveTab = TabItem3;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buEndClick(TObject *Sender)
{
    tc->ActiveTab = TabItem4;
}
//---------------------------------------------------------------------------

