//---------------------------------------------------------------------------

#ifndef frameContactH
#define frameContactH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TfruContact : public TFrame
{
__published:	// IDE-managed Components
	TListBox *ListBox1;
	TListBoxItem *lbiNumber;
	TListBoxItem *lbiEmail;
	TListBoxItem *lbiURL;
	TEdit *edNumber;
	TEdit *edEmail;
	TEdit *edURL;
private:	// User declarations
public:		// User declarations
	__fastcall TfruContact(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfruContact *fruContact;
//---------------------------------------------------------------------------
#endif
