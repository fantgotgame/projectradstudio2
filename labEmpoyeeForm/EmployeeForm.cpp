//---------------------------------------------------------------------------

#include <fmx.h>
#ifdef _WIN32
#include <tchar.h>
#endif
#pragma hdrstop
#include <System.StartUpCopy.hpp>
//---------------------------------------------------------------------------
USEFORM("framePerson.cpp", fruPerson); /* TFrame: File Type */
USEFORM("frameStart.cpp", fruStart); /* TFrame: File Type */
USEFORM("labEmployeeForm.cpp", fm);
USEFORM("frameEnd.cpp", fruEnd); /* TFrame: File Type */
USEFORM("frameContact.cpp", fruContact); /* TFrame: File Type */
//---------------------------------------------------------------------------
extern "C" int FMXmain()
{
	try
	{
		Application->Initialize();
		Application->CreateForm(__classid(Tfm), &fm);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
