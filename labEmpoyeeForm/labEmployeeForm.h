//---------------------------------------------------------------------------

#ifndef labEmployeeFormH
#define labEmployeeFormH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include "frameContact.h"
#include "frameEnd.h"
#include "framePerson.h"
#include "frameStart.h"
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TfruStart *fruStart1;
	TfruPerson *fruPerson1;
	TfruContact *fruContact1;
	TfruEnd *fruEnd1;
	TButton *buNext1;
	TButton *buEnd;
	void __fastcall fruEnd1buMoreClick(TObject *Sender);
	void __fastcall fruStart1buYesClick(TObject *Sender);
	void __fastcall fruStart1buNoClick(TObject *Sender);
	void __fastcall fruEnd1buExitClick(TObject *Sender);
	void __fastcall buNext1Click(TObject *Sender);
	void __fastcall buEndClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
