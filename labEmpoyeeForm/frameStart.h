//---------------------------------------------------------------------------

#ifndef frameStartH
#define frameStartH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TfruStart : public TFrame
{
__published:	// IDE-managed Components
	TLabel *laQuestion;
	TButton *buYes;
	TButton *buNo;
private:	// User declarations
public:		// User declarations
	__fastcall TfruStart(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfruStart *fruStart;
//---------------------------------------------------------------------------
#endif
