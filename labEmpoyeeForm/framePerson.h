//---------------------------------------------------------------------------

#ifndef framePersonH
#define framePersonH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TfruPerson : public TFrame
{
__published:	// IDE-managed Components
	TListBox *ListBox1;
	TListBoxItem *lbSecondName;
	TListBoxItem *lbiFirstName;
	TListBoxItem *lbiPatronymic;
	TListBoxItem *lbMark;
	TEdit *edSecondName;
	TEdit *edFirstName;
	TEdit *edPatronymic;
	TEdit *edMark;
private:	// User declarations
public:		// User declarations
	__fastcall TfruPerson(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfruPerson *fruPerson;
//---------------------------------------------------------------------------
#endif
