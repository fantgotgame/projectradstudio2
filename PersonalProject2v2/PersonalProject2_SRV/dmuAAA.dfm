object dmAAA: TdmAAA
  OldCreateOrder = False
  Height = 332
  Width = 459
  object HeroesConnection: TSQLConnection
    ConnectionName = 'HEROS'
    DriverName = 'Firebird'
    LoginPrompt = False
    Params.Strings = (
      'GetDriverFunc=getSQLDriverINTERBASE'
      'DriverName=Firebird'
      'DriverUnit=Data.DBXFirebird'
      
        'DriverPackageLoader=TDBXDynalinkDriverLoader,DbxCommonDriver240.' +
        'bpl'
      
        'DriverAssemblyLoader=Borland.Data.TDBXDynalinkDriverLoader,Borla' +
        'nd.Data.DbxCommonDriver,Version=24.0.0.0,Culture=neutral,PublicK' +
        'eyToken=91d62ebb5b0d1b1b'
      
        'MetaDataPackageLoader=TDBXFirebirdMetaDataCommandFactory,DbxFire' +
        'birdDriver240.bpl'
      
        'MetaDataAssemblyLoader=Borland.Data.TDBXFirebirdMetaDataCommandF' +
        'actory,Borland.Data.DbxFirebirdDriver,Version=24.0.0.0,Culture=n' +
        'eutral,PublicKeyToken=91d62ebb5b0d1b1b'
      'LibraryName=dbxfb.dll'
      'LibraryNameOsx=libsqlfb.dylib'
      'VendorLib=fbclient.dll'
      'VendorLibWin64=fbclient.dll'
      'VendorLibOsx=/Library/Frameworks/Firebird.framework/Firebird'
      'Database=C:\Users\VD\Desktop\HEROS.FDB'
      'User_Name=sysdba'
      'Password=masterkey'
      'Role=RoleName'
      'MaxBlobSize=-1'
      'LocaleCode=0000'
      'IsolationLevel=ReadCommitted'
      'SQLDialect=3'
      'CommitRetain=False'
      'WaitOnLocks=True'
      'TrimChar=False'
      'BlobSize=-1'
      'ErrorResourceFile='
      'RoleName=RoleName'
      'ServerCharSet=UTF8'
      'Trim Char=False')
    AfterConnect = HeroesConnectionAfterConnect
    Left = 116
    Top = 44
  end
  object taHeros: TSQLDataSet
    CommandText = 'HEROS'
    CommandType = ctTable
    DbxCommandType = 'Dbx.Table'
    MaxBlobSize = 1
    Params = <>
    SQLConnection = HeroesConnection
    Left = 108
    Top = 100
    object taHerosID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object taHerosNAME: TWideStringField
      FieldName = 'NAME'
      Size = 160
    end
    object taHerosTYPE: TWideStringField
      FieldName = 'TYPE'
      Size = 80
    end
    object taHerosROLE: TWideStringField
      FieldName = 'ROLE'
      Size = 80
    end
    object taHerosTYPEATTACK: TWideStringField
      FieldName = 'TYPEATTACK'
      Size = 80
    end
    object taHerosIMAGE: TBlobField
      FieldName = 'IMAGE'
      Size = 1
    end
  end
  object dspHeros: TDataSetProvider
    DataSet = taHeros
    Left = 104
    Top = 160
  end
end
