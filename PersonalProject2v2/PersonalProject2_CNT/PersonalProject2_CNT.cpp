//---------------------------------------------------------------------------

#include <fmx.h>
#ifdef _WIN32
#include <tchar.h>
#endif
#pragma hdrstop
#include <System.StartUpCopy.hpp>
//---------------------------------------------------------------------------
USEFORM("fmuTypes.cpp", fmTypes);
USEFORM("fmTypesOfAttack.cpp", fTypesOfAttack);
USEFORM("dmu.cpp", dm); /* TDataModule: File Type */
USEFORM("fmuRoles.cpp", fmRole);
USEFORM("fmu.cpp", fm);
//---------------------------------------------------------------------------
extern "C" int FMXmain()
{
	try
	{
		Application->Initialize();
		Application->CreateForm(__classid(Tfm), &fm);
		Application->CreateForm(__classid(Tdm), &dm);
		Application->CreateForm(__classid(TfmTypes), &fmTypes);
		Application->CreateForm(__classid(TfmRole), &fmRole);
		Application->CreateForm(__classid(TfTypesOfAttack), &fTypesOfAttack);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
