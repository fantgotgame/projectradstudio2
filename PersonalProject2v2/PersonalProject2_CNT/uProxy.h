#ifndef uProxyH
#define uProxyH

#include "Data.DBXCommon.hpp"
#include "System.Classes.hpp"
#include "System.SysUtils.hpp"
#include "Data.DB.hpp"
#include "Data.SqlExpr.hpp"
#include "Data.DBXDBReaders.hpp"
#include "Data.DBXCDSReaders.hpp"

  class TdmAAAClient : public TObject
  {
  private:
    TDBXConnection *FDBXConnection;
    bool FInstanceOwner;
    TDBXCommand *FHeroesConnectionAfterConnectCommand;
    TDBXCommand *FEchoStringCommand;
    TDBXCommand *FReverseStringCommand;
  public:
    __fastcall TdmAAAClient(TDBXConnection *ADBXConnection);
    __fastcall TdmAAAClient(TDBXConnection *ADBXConnection, bool AInstanceOwner);
    __fastcall ~TdmAAAClient();
    void __fastcall HeroesConnectionAfterConnect(TJSONObject* Sender);
    System::UnicodeString __fastcall EchoString(System::UnicodeString value);
    System::UnicodeString __fastcall ReverseString(System::UnicodeString value);
  };

#endif
