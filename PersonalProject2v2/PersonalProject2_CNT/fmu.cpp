//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
#include "fmuTypes.h"
#include "fmuRoles.h"
#include "fmTypesOfAttack.h"
//#include "dmuAAA.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall Tfm::lwItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	laHeroName->Text = dm->cdsHEROSNAME->Value;
	imHero->Bitmap->Assign(dm->cdsHEROSIMAGE);
	laType->Text = "Type of Hero: " + dm->cdsHEROSTYPE->Value;
	laRole->Text = "Hero role: " + dm->cdsHEROSROLE->Value;
	laTypeOfAttack->Text = "Type Attack: " + dm->cdsHEROSTYPEATTACK->Value;
	tc->GotoVisibleTab(tiDetails->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::FormCreate(TObject *Sender)
{
    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackMenuClick(TObject *Sender)
{
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAboutClick(TObject *Sender)
{
	tc->ActiveTab = tiAbout;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buExitClick(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackClick(TObject *Sender)
{
    tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buBackToHeroesClick(TObject *Sender)
{
    tc->ActiveTab = tiHeroes;
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buEnterClick(TObject *Sender)
{
	tc->ActiveTab = tiHeroes;
}
//---------------------------------------------------------------------------


void __fastcall Tfm::buDetails1Click(TObject *Sender)
{
	fmTypes->Show();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDetails2Click(TObject *Sender)
{
	fmRole->Show();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDetails3Click(TObject *Sender)
{
	fTypesOfAttack->Show();
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buAboutMakesClick(TObject *Sender)
{
    ShowMessage("Fateev Ilya. Gp. 151-362");
}
//---------------------------------------------------------------------------

