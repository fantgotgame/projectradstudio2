//---------------------------------------------------------------------------

#ifndef fmTypesOfAttackH
#define fmTypesOfAttackH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TfTypesOfAttack : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label2;
	TLabel *laStrength;
	TMemo *Memo2;
	TMemo *Memo3;
	TToolBar *ToolBar2;
	TLabel *laRole;
	TStyleBook *StyleBook1;
private:	// User declarations
public:		// User declarations
	__fastcall TfTypesOfAttack(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfTypesOfAttack *fTypesOfAttack;
//---------------------------------------------------------------------------
#endif
