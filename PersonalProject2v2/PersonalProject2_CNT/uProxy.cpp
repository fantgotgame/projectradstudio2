// 
// Created by the DataSnap proxy generator.
// 09.01.2018 12:30:47
// 

#include "uProxy.h"

void __fastcall TdmAAAClient::HeroesConnectionAfterConnect(TJSONObject* Sender)
{
  if (FHeroesConnectionAfterConnectCommand == NULL)
  {
    FHeroesConnectionAfterConnectCommand = FDBXConnection->CreateCommand();
    FHeroesConnectionAfterConnectCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FHeroesConnectionAfterConnectCommand->Text = "TdmAAA.HeroesConnectionAfterConnect";
    FHeroesConnectionAfterConnectCommand->Prepare();
  }
  FHeroesConnectionAfterConnectCommand->Parameters->Parameter[0]->Value->SetJSONValue(Sender, FInstanceOwner);
  FHeroesConnectionAfterConnectCommand->ExecuteUpdate();
}

System::UnicodeString __fastcall TdmAAAClient::EchoString(System::UnicodeString value)
{
  if (FEchoStringCommand == NULL)
  {
    FEchoStringCommand = FDBXConnection->CreateCommand();
    FEchoStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FEchoStringCommand->Text = "TdmAAA.EchoString";
    FEchoStringCommand->Prepare();
  }
  FEchoStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FEchoStringCommand->ExecuteUpdate();
  System::UnicodeString result = FEchoStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}

System::UnicodeString __fastcall TdmAAAClient::ReverseString(System::UnicodeString value)
{
  if (FReverseStringCommand == NULL)
  {
    FReverseStringCommand = FDBXConnection->CreateCommand();
    FReverseStringCommand->CommandType = TDBXCommandTypes_DSServerMethod;
    FReverseStringCommand->Text = "TdmAAA.ReverseString";
    FReverseStringCommand->Prepare();
  }
  FReverseStringCommand->Parameters->Parameter[0]->Value->SetWideString(value);
  FReverseStringCommand->ExecuteUpdate();
  System::UnicodeString result = FReverseStringCommand->Parameters->Parameter[1]->Value->GetWideString();
  return result;
}


__fastcall  TdmAAAClient::TdmAAAClient(TDBXConnection *ADBXConnection)
{
  if (ADBXConnection == NULL)
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = True;
}


__fastcall  TdmAAAClient::TdmAAAClient(TDBXConnection *ADBXConnection, bool AInstanceOwner)
{
  if (ADBXConnection == NULL) 
    throw EInvalidOperation("Connection cannot be nil.  Make sure the connection has been opened.");
  FDBXConnection = ADBXConnection;
  FInstanceOwner = AInstanceOwner;
}


__fastcall  TdmAAAClient::~TdmAAAClient()
{
  delete FHeroesConnectionAfterConnectCommand;
  delete FEchoStringCommand;
  delete FReverseStringCommand;
}

