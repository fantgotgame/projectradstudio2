//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TLabel *laCaption;
	TTabItem *tiMenu;
	TGridPanelLayout *gpl;
	TButton *buAbout;
	TButton *buExit;
	TImage *im;
	TLayout *lyMenu;
	TButton *buEnter;
	TTabItem *tiAbout;
	TToolBar *tbAbout;
	TLabel *laAbout;
	TButton *buBackMenu;
	TTabItem *tiHeroes;
	TToolBar *tb;
	TLabel *laTb;
	TImage *Image1;
	TMemo *Memo1;
	TButton *buBack;
	TScrollBox *sb;
	TListView *lw;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TTabItem *tiDetails;
	TToolBar *ToolBar1;
	TLabel *laHeroName;
	TButton *buBackToHeroes;
	TImage *imHero;
	TLabel *laType;
	TLabel *laTypeOfAttack;
	TLabel *laRole;
	TImage *Image2;
	TButton *buDetails1;
	TButton *buDetails2;
	TButton *buDetails3;
	TButton *buAboutMakes;
	TStyleBook *StyleBook1;
	void __fastcall lwItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buBackMenuClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buExitClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall buBackToHeroesClick(TObject *Sender);
	void __fastcall buEnterClick(TObject *Sender);
	void __fastcall buDetails1Click(TObject *Sender);
	void __fastcall buDetails2Click(TObject *Sender);
	void __fastcall buDetails3Click(TObject *Sender);
	void __fastcall buAboutMakesClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
