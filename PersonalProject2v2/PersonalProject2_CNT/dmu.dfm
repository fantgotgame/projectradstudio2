object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 271
  Width = 415
  object SQLConnection1: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverName=DataSnap'
      'HostName=localhost'
      'port=211')
    Connected = True
    Left = 80
    Top = 40
    UniqueId = '{463090C4-0E44-46BA-8ACB-0EDBF6C2161E}'
  end
  object DSProviderConnection1: TDSProviderConnection
    ServerClassName = 'TdmAAA'
    Connected = True
    SQLConnection = SQLConnection1
    Left = 168
    Top = 96
  end
  object cdsHEROS: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspHeros'
    RemoteServer = DSProviderConnection1
    Left = 112
    Top = 176
    object cdsHEROSID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsHEROSNAME: TWideStringField
      FieldName = 'NAME'
      Size = 160
    end
    object cdsHEROSTYPE: TWideStringField
      FieldName = 'TYPE'
      Size = 80
    end
    object cdsHEROSROLE: TWideStringField
      FieldName = 'ROLE'
      Size = 80
    end
    object cdsHEROSTYPEATTACK: TWideStringField
      FieldName = 'TYPEATTACK'
      Size = 80
    end
    object cdsHEROSIMAGE: TBlobField
      FieldName = 'IMAGE'
      Size = 1
    end
  end
end
