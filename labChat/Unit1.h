//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <IPPeerClient.hpp>
#include <IPPeerServer.hpp>
#include <System.Tether.AppProfile.hpp>
#include <System.Tether.Manager.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TEdit *edText;
	TEdit *edName;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buSend;
	TMemo *me;
	TTetheringManager *ttm;
	TTetheringAppProfile *ttp;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall ttmPairedFromLocal(TObject * const Sender, const TTetheringManagerInfo &AManagerInfo);
	void __fastcall ttmPairedToRemote(TObject * const Sender, const TTetheringManagerInfo &AManagerInfo);
	void __fastcall buSendClick(TObject *Sender);
	void __fastcall ttpResourceReceived(TObject * const Sender, TRemoteResource * const AResource);



private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
