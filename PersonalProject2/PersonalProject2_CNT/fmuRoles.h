//---------------------------------------------------------------------------

#ifndef fmuRolesH
#define fmuRolesH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TfmRole : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label2;
	TLabel *Label3;
	TLabel *laStrength;
	TMemo *Memo2;
	TMemo *Memo3;
	TMemo *Memo4;
	TToolBar *ToolBar2;
	TLabel *laRole;
	TStyleBook *StyleBook1;
private:	// User declarations
public:		// User declarations
	__fastcall TfmRole(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfmRole *fmRole;
//---------------------------------------------------------------------------
#endif
