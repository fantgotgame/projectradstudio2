//---------------------------------------------------------------------------

#ifndef fmuTypesH
#define fmuTypesH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TfmTypes : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label2;
	TLabel *Label3;
	TLabel *laStrength;
	TMemo *Memo2;
	TMemo *Memo3;
	TMemo *Memo4;
	TToolBar *ToolBar2;
	TLabel *Label1;
	TStyleBook *StyleBook1;
private:	// User declarations
public:		// User declarations
	__fastcall TfmTypes(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfmTypes *fmTypes;
//---------------------------------------------------------------------------
#endif
