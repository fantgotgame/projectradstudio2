object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 271
  Width = 415
  object SQLConnection1: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXDataSnap'
      'HostName=localhost'
      
        'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland' +
        '.Data.DbxClientDriver,Version=24.0.0.0,Culture=neutral,PublicKey' +
        'Token=91d62ebb5b0d1b1b'
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/'
      'Filters={}')
    Connected = True
    Left = 48
    Top = 40
  end
  object DSProviderConnection1: TDSProviderConnection
    ServerClassName = 'TdmAAA'
    Connected = True
    SQLConnection = SQLConnection1
    Left = 168
    Top = 96
  end
  object cdsHEROS: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspHeros'
    RemoteServer = DSProviderConnection1
    Left = 112
    Top = 176
    object cdsHEROSID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object cdsHEROSNAME: TWideStringField
      FieldName = 'NAME'
      Size = 160
    end
    object cdsHEROSTYPE: TWideStringField
      FieldName = 'TYPE'
      Size = 80
    end
    object cdsHEROSROLE: TWideStringField
      FieldName = 'ROLE'
      Size = 80
    end
    object cdsHEROSTYPEATTACK: TWideStringField
      FieldName = 'TYPEATTACK'
      Size = 80
    end
    object cdsHEROSIMAGE: TBlobField
      FieldName = 'IMAGE'
      Size = 1
    end
  end
end
