//---------------------------------------------------------------------------

#pragma hdrstop

#include "dmuAAA.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TdmAAA::TdmAAA(TComponent* Owner)
	: TDSServerModule(Owner)
{
}
//----------------------------------------------------------------------------
System::UnicodeString TdmAAA::EchoString(System::UnicodeString value)
{
    return value;
}
//----------------------------------------------------------------------------
System::UnicodeString TdmAAA::ReverseString(System::UnicodeString value)
{
    return ::ReverseString(value);
}
//----------------------------------------------------------------------------


