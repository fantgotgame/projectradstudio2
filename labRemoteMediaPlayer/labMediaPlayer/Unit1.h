//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Media.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.ActnList.hpp>
#include <FMX.Dialogs.hpp>
#include <FMX.StdActns.hpp>
#include <IPPeerClient.hpp>
#include <IPPeerServer.hpp>
#include <System.Actions.hpp>
#include <System.Tether.AppProfile.hpp>
#include <System.Tether.Manager.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TMemo *meLog;
	TTrackBar *tbVolume;
	TToolBar *ToolBar1;
	TButton *buOpen;
	TButton *buPlay;
	TButton *buStop;
	TButton *buTitle;
	TButton *buClear;
	TLayout *Layout1;
	TLabel *laVolume;
	TLabel *laVolumeCaption;
	TLayout *Layout2;
	TTrackBar *tbTime;
	TLabel *LaDuration;
	TLabel *laTime;
	TMediaPlayerControl *mpc;
	TMediaPlayer *mp;
	TTetheringManager *ttm;
	TTetheringAppProfile *ttp;
	TActionList *al;
	TOpenDialog *od;
	TMediaPlayerPlayPause *acMediaPlayerPlayPause;
	TMediaPlayerStop *acMediaPlayerStop;
	TMediaPlayerCurrentTime *MediaPlayerCurrentTime1;
	TMediaPlayerVolume *MediaPlayerVolume1;
	TAction *acClear;
	TAction *acPlayPause;
	TAction *acStop;
	void __fastcall buOpenClick(TObject *Sender);
	void __fastcall acPlayPauseExecute(TObject *Sender);
	void __fastcall acStopExecute(TObject *Sender);
	void __fastcall tbVolumeChange(TObject *Sender);
	void __fastcall tbTimeChange(TObject *Sender);
	void __fastcall ttpResourceReceived(TObject * const Sender, TRemoteResource * const AResource);



private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
