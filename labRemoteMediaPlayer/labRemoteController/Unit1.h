//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.ActnList.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <IPPeerClient.hpp>
#include <IPPeerServer.hpp>
#include <System.Actions.hpp>
#include <System.Tether.AppProfile.hpp>
#include <System.Tether.Manager.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buFind;
	TLabel *laPlayers;
	TLabel *Label1;
	TLayout *Layout1;
	TButton *buPLay;
	TButton *buStop;
	TButton *buClear;
	TLayout *Layout2;
	TLabel *Label2;
	TTrackBar *tbVolume;
	TLayout *Layout3;
	TLabel *Label3;
	TTrackBar *tbTime;
	TEdit *edString;
	TButton *buSendString;
	TTetheringManager *ttm;
	TActionList *al;
	TTetheringAppProfile *ttp;
	TAction *acPlayPause;
	TAction *acStop;
	TAction *acClear;
	TListBox *lb;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall buFindClick(TObject *Sender);
	void __fastcall ttmEndProfilesDiscovery(TObject * const Sender, TTetheringProfileInfoList * const ARemoteProfiles);
	void __fastcall ttmEndManagersDiscovery(TObject * const Sender, TTetheringManagerInfoList * const ARemoteManagers);
	void __fastcall ttpResources0ResourceReceived(TObject * const Sender, TRemoteResource * const AResource);
	void __fastcall ttpResources1ResourceReceived(TObject * const Sender, TRemoteResource * const AResource);
	void __fastcall ttpResources2ResourceReceived(TObject * const Sender, TRemoteResource * const AResource);
	void __fastcall ttpResources3ResourceReceived(TObject * const Sender, TRemoteResource * const AResource);
	void __fastcall buSendStringClick(TObject *Sender);








private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
