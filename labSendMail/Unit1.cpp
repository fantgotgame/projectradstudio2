//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buSendClick(TObject *Sender)
{
    IdSMTP1->Host = "smtp.mail.ru";
	IdSMTP1->Port = 587;
	IdSMTP1->Username = "test123210@mail.ru";
	IdSMTP1->Password = "Pa$$w0rd";
	IdSMTP1->UseTLS = utUseExplicitTLS;
	IdSMTP1->ReadTimeout = 15000;
	IdSMTP1->Connect ();

	if (IdSMTP1->Connected () == true)
	{
		TIdMessage *x = new TIdMessage ();

		try
		{
			x->From->Address = IdSMTP1->Username;
			x->Recipients->Add ()->Address = Trim (edTo->Text);
			x->Subject = Trim (edSubject->Text);
			x->Body->Assign (meBody->Lines);
			x->CharSet = "Windows-1251";
			if (cbIncludeAttach->IsChecked)
			{
				UnicodeString xFileName = Ioutils::TPath::ChangeExtension (Ioutils::TPath::GetTempFileNameW (), ".txt");
				meBody->Lines->SaveToFile (xFileName, TEncoding::UTF8);
				new TIdAttachmentFile (x->MessageParts, xFileName);
			}
			IdSMTP1->Send (x);
			ShowMessage (L"������ ����������");
		}
		__finally
		{
			delete x;
			IdSMTP1->Disconnect ();
        }
	}
}
//---------------------------------------------------------------------------
