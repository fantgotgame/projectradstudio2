//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include <System.IOUtils.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buConnectClick(TObject *Sender)
{
	IdFTP->Host = "46.188.34.242";
	IdFTP->Username = "test";
	IdFTP->Password = "test";
	IdFTP->Passive = True;
	IdFTP->Connect();
	IdFTP->ChangeDir("/ftp/");
	IdFTP->List(lb->Items, "", false);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lbItemClick(TCustomListBox * const Sender, TListBoxItem * const Item)

{
	UnicodeString xExt = Ioutils::TPath::GetExtension(Item->Text);
	UnicodeString xIt = Item->Name;
	TMemoryStream *x = new TMemoryStream();
	try {
		if (xExt == ".txt") {
			tc->ActiveTab = tiText;
			IdFTP->Get(Item->Text, x, true);
			x->Seek(0,0);
			me->Lines->LoadFromStream(x);
		} else
		if (xExt == ".png" || xExt == ".jpg") {
			tc->ActiveTab = tiImage;
			IdFTP->Get(Item->Text, x, true);
			im->Bitmap->LoadFromStream(x);
		}else
			tc->ActiveTab = tiOther;
	}
	__finally {
			delete x;
	}
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buDownClick(TObject *Sender)
{
	if (xExt == ".png"){
		im->Bitmap->SaveToFile("D:/RAD/Картинки/"+xIt+".png");
	}
	else
	if (xExt == ".jpg") {
		im->Bitmap->SaveToFile("D:/RAD/Картинки/"+xIt+".jpg");
	}
}

//---------------------------------------------------------------------------

