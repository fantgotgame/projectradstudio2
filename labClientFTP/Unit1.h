//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.Memo.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdExplicitTLSClientServerBase.hpp>
#include <IdFTP.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *ly;
	TGridPanelLayout *gpl;
	TIdFTP *IdFTP;
	TListBox *lb;
	TButton *buConnect;
	TTabControl *tc;
	TTabItem *tiImage;
	TTabItem *tiText;
	TTabItem *tiOther;
	TMemo *me;
	TImage *im;
	TButton *buDown;
	void __fastcall buConnectClick(TObject *Sender);
	void __fastcall lbItemClick(TCustomListBox * const Sender, TListBoxItem * const Item);
	void __fastcall buDownClick(TObject *Sender);

private:	// User declarations
public:	UnicodeString xExt;
		UnicodeString xIt;	// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
